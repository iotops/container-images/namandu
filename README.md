# What is Ñamandú

Is the main god of the *Guaraní* mithology, the first, the origin and the beginning. 

He lives in Yvága, our IT solution, aka our laptops and gitlab.com. 

## How can Ñamandú build things?

Speaking technician, Ñamandú today takes a git repo with a Dockerfile definition, builds it, and then publish the resultant image to the Docker Hub Registry. 

## How can I invoke his power?


You should use the creator word *Ayvú*, aka add entries to the `projects.yaml` file, with the following fields:

```yaml
- name: docker-curl 
  url: https://github.com/appropriate/docker-curl.git
  archs:
    - amd64
    - arm
  changes: |
    echo no changes to apply
    date
  dockerfile: latest/Dockerfile
  context: latest
```
- name: name of the image to be built, this name should be the same used on the Docker Hub Repository
- url: https git repository url
- archs: architectures to be supported by the image (currently amd64 and arm)
- changes: custom script that can be used to apply changes on the code, things like `sed` to manipulate the code should be here. 
- dockerfile: relative path to the Dockerfile file. 
- context: context to be used (for example a subdirectory)

In addition, if you want to use a private git repository, you should provide Ñamandú with credentials in the form of gitlab's environment variables, taking the name's value in the form NAMEUSERNAME and NAMEPASS, if the name has a -, you should remove it. 

For example, for `name: docker-curl`, the variables should be:

```sh
DOCKERCURLUSERNAME=XXXXXXX
DOCKERCURLPASS=XXXXX

```


